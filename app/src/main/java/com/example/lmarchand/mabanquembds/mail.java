package com.example.lmarchand.mabanquembds;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class mail extends AppCompatActivity {
    private EditText mEditTextTO;
    private EditText mEditTextSubject;
    private EditText mEditTextMessage;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mail);

        mEditTextTO = findViewById(R.id.editText_to);
        mEditTextSubject = findViewById(R.id.editText_subject);
        mEditTextMessage = findViewById(R.id.editText_message);

        Button buttonSend = findViewById(R.id.button_send);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMail();
            }
        });

    }

    private void sendMail(){
        String recipientList = mEditTextTO.getText().toString();
        String[] recipients = recipientList.split(",");
        //0000@gmail.com, 1111@gmail.com

        String subject = mEditTextSubject.getText().toString();
        String message = mEditTextMessage.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"l.zhao@net.estia.fr"});
        //intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);

        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Choose an email client"));


    }
}
