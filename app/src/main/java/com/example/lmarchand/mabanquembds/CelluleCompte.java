package com.example.lmarchand.mabanquembds;

public class CelluleCompte {
    private String nom;
    private String iban;
    private String solde;

    public CelluleCompte(String nom, String iban, String solde) {
        this.nom = nom;
        this.iban = iban;
        this.solde = solde;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getSolde() {
        return solde;
    }

    public void setSolde(String solde) {
        this.solde = solde;
    }
}
