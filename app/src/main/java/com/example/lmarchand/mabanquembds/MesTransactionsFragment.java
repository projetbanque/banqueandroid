package com.example.lmarchand.mabanquembds;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.example.lmarchand.mabanquembds.paramsWebService.IDENTIFIANT_CLIENT;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_COMPTEBANCAIRE;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_DATETRANSFERT;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_IBANCB;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_IDCOMPTECREDIT;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_IDCOMPTEDEBIT;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_LIBELLE;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_NOM;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_SOMMETRANSFEREE;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_TRANSACTIONARGENT;
import static com.example.lmarchand.mabanquembds.paramsWebService.MOTDEPASSE_CLIENT;
import static com.example.lmarchand.mabanquembds.paramsWebService.URL_START;


public class MesTransactionsFragment extends Fragment {
    ListView listTransaction;

    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_mes_transactions, container,false);

       listTransaction = (ListView)  myView.findViewById(R.id.listViewTransaction);

       afficherListeTransaction();

        return myView;
    }


    private List<CelluleTransaction> genererTransaction(){
        List<CelluleTransaction> transactions = new ArrayList<CelluleTransaction>();
        //public CelluleTransaction(String nom, String compte, String nomA, String compteA, String date, String argent, String libelle)
        transactions.add(new CelluleTransaction( "FAKE Florent", "FAKE HGVIYTFCHVKG3","FAKE Thomas","FAKE TFYDFYRDRTKUYG","FAKE 12/12/2018","FAKE 50","FAKE co-voiturage Paris"));
        return transactions;
    }

    private void afficherListeTransaction(){
        try {
            //requete webservice
            XMLParser parser = new XMLParser();
            String xml = parser.getXml(URL_START+"transactions/transactionclient/"+IDENTIFIANT_CLIENT+"/"+MOTDEPASSE_CLIENT); // getting XML
            Document doc = parser.getDomElement(xml); // getting DOM element

            NodeList nl = doc.getElementsByTagName(KEY_TRANSACTIONARGENT);
            List<CelluleTransaction> transactions = new ArrayList<CelluleTransaction>();
            // looping through all item nodes <item>
            for (int i = 0; i < nl.getLength(); i++) {
                Element e = (Element) nl.item(i);

                NodeList nlDebit = doc.getElementsByTagName(KEY_IDCOMPTEDEBIT);
                Element eDebit = (Element) nlDebit.item(i);

                NodeList nlCredit = doc.getElementsByTagName(KEY_IDCOMPTECREDIT);
                Element eCredit = (Element) nlCredit.item(i);

                //Collections.reverse(transactions);

                transactions.add(new CelluleTransaction( parser.getValue(eDebit,KEY_NOM), parser.getValue(eDebit,KEY_IBANCB),parser.getValue(eCredit,KEY_NOM),parser.getValue(eCredit,KEY_IBANCB), parser.getValue(e, KEY_DATETRANSFERT).substring(0, 10),parser.getValue(e,KEY_SOMMETRANSFEREE),parser.getValue(e,KEY_LIBELLE)));

            }

            CelluleTransactionAdapter adapter = new CelluleTransactionAdapter(getContext(),transactions);
            listTransaction.setAdapter(adapter);
        }catch (Exception e){
            Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
            List<CelluleTransaction> transactions = genererTransaction();
            CelluleTransactionAdapter adapter = new CelluleTransactionAdapter(getContext(), transactions);
            listTransaction.setAdapter(adapter);
        }


    }

}
