package com.example.lmarchand.mabanquembds;

import android.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lmarchand.mabanquembds.models.Client;
import com.example.lmarchand.mabanquembds.service.RequestHelper;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.lmarchand.mabanquembds.paramsWebService.*;

public class MonProfilModifActivity extends AppCompatActivity {

    Button buttonEnregistre;
    TextView textViewNom;
    TextView textViewPays;
    EditText editTextPays;
    TextView textViewMail;
    EditText editTextMail;
    TextView textViewNumeroT;
    EditText editTextNumeroT;
    TextView textViewMDP;
    EditText editTextMDP;
    TextView textViewNais;
    TextView textViewIden;
    EditText editTextIden;
    TextView textViewAddres;
    EditText editTextAddres;

    String resultat;

    Client client = new Client();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mon_profil_modif);

        buttonEnregistre = (Button) findViewById(R.id.buttonEnregistre);
        buttonEnregistre.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if (editTextIden.getText().toString().equals("")||editTextMDP.getText().toString().equals("")||editTextMail.getText().toString().equals("")||editTextNumeroT.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Champs manquant !",Toast.LENGTH_LONG).show();
                }else{
                    client.setIdentifiant(editTextIden.getText().toString());
                    client.setMotdepasse(editTextMDP.getText().toString());
                    client.setEmail(editTextMail.getText().toString());
                    client.setTelephone(editTextNumeroT.getText().toString());
                    //update profil dans Base De Données
                    //METHODE PUT
                    //...
                    RequestHelper.provideService().editClient(IDENTIFIANT_CLIENT,MOTDEPASSE_CLIENT, client).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Toast.makeText(getApplicationContext(),"Profil modifié",Toast.LENGTH_LONG).show();
                            IDENTIFIANT_CLIENT=client.getIdentifiant();
                            MOTDEPASSE_CLIENT=client.getMotdepasse();
                            startActivity(new Intent(MonProfilModifActivity.this,EspaceClientActivity.class));
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });

                }
           }
        });


        try {
            //Appel web service
            XMLParser parser = new XMLParser();
            String xml = parser.getXml(URL_LOGIN + IDENTIFIANT_CLIENT + "/" + MOTDEPASSE_CLIENT); // getting XML
            Document doc = parser.getDomElement(xml); // getting DOM element

            NodeList nl = doc.getElementsByTagName(KEY_CLIENT);
            Element e = (Element) nl.item(0);

            textViewNom = (TextView) findViewById(R.id.textViewNom);
            textViewNom.setText(parser.getValue(e, KEY_NOM) + " " + parser.getValue(e, KEY_PRENOM));


            editTextMail = (EditText) findViewById(R.id.editTextMail);
            editTextMail.setText(parser.getValue(e, KEY_EMAIL));

            editTextIden = (EditText) findViewById(R.id.editTextIden);
            editTextIden.setText(parser.getValue(e, KEY_IDENTIFIANT));

            editTextMDP = (EditText) findViewById(R.id.editTextMDP);
            editTextMDP.setText(parser.getValue(e, KEY_MOTDEPASSE));

            editTextNumeroT = (EditText) findViewById(R.id.editTextNumeroT);
            editTextNumeroT.setText(parser.getValue(e, KEY_NUMEROT));

            textViewNais = (TextView) findViewById(R.id.textViewNais);
            textViewNais.setText(parser.getValue(e, KEY_DATEDENAISSANCE).substring(0, 10));

            textViewAddres = (TextView) findViewById(R.id.textViewAddres);
            textViewAddres.setText(parser.getValue(e, KEY_NUMERORUE) + " " + parser.getValue(e, KEY_RUE) + " " + parser.getValue(e, KEY_CODEPOSTAL) + " " + parser.getValue(e, KEY_VILLE));

            textViewPays = (TextView) findViewById(R.id.textViewPays);
            textViewPays.setText(parser.getValue(e, KEY_PAYS));
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
        }

    }
}
