package com.example.lmarchand.mabanquembds;

public class paramsWebService {
    //IPV4 Lu
    //public static final String IPV4 ="172.20.10.2";
    //IPV4 wifi maison Lore
    //public static final String IPV4 ="192.168.1.32";
    //IPV4 telephone Lore
    public static final String IPV4 ="192.168.43.68";

    //URL
    public static final String URL_START="http://"+IPV4+":8080/WebServiceYinhanBanqueMarchandZhao/webresources/";

    //URL pour verifier identifiants client
    static final String URL_LOGIN = URL_START+"ws_entity.client/login/";
    public static String IDENTIFIANT_CLIENT;
    public static String MOTDEPASSE_CLIENT;
    public static String NOM_CLIENT;
    public static String PRENOM_CLIENT;

    //URL du profil du client une fois le client connecté
    //public static String URL_CLIENT = URL_LOGIN+IDENTIFIANT_CLIENT+"/"+MOTDEPASSE_CLIENT;
    //URL du profil du client une fois le client connecté
    //public static String URL_COMPTECLIENT = URL_START+"ws_entity.comptebancaire/compteclient/"+IDENTIFIANT_CLIENT+"/"+MOTDEPASSE_CLIENT;
    //URL des transactions du client une fois le client connecté
    //public static String URL_TRANSACTIONCLIENT = URL_START+"ws_entity.transactionargent/transactionclient/"+IDENTIFIANT_CLIENT+"/"+MOTDEPASSE_CLIENT;
    //URL des sauvegarde de compte beneficiaire du client une fois le client connecté
    //public static String URL_SAUVEGARDECOMPTEBENEFICIAIRECLIENT = URL_START+"ws_entity.sauvegardecomprebeneficiaire/comptebeneficiaireclient/"+IDENTIFIANT_CLIENT+"/"+MOTDEPASSE_CLIENT;

    // XML node keys SAUVEGARDE COMPTE BENEFICIAIRE
    static final String KEY_SAUVEGARDECOMPTEBENEFICIAIRES = "sauvegardecomprebeneficiaires"; // parent node
    static final String KEY_SAUVEGARDECOMPTEBENEFICIAIRE = "sauvegardecomprebeneficiaire";
    static final String KEY_NOMSCB = "nomscb";


    // XML node keys TRANSACTION
    static final String KEY_TRANSACTIONARGENTS = "transactionargents"; // parent node
    static final String KEY_TRANSACTIONARGENT = "transactionargent";
    static final String KEY_DATETRANSFERT = "datetransfert";
    static final String KEY_IDCOMPTECREDIT = "idComptecredit";
    static final String KEY_IDCOMPTEDEBIT = "idComptedebit";
    static final String KEY_LIBELLE = "libelle";
    static final String KEY_SOMMETRANSFEREE = "sommetransferee";

    // XML node keys COMPTE
    static final String KEY_COMPTEBANCAIRES = "comptebancaires"; // parent node
    static final String KEY_COMPTEBANCAIRE = "comptebancaire";
    static final String KEY_NOMCB = "nomcb";
    static final String KEY_IBANCB = "ibancb";
    static final String KEY_SOLDECB = "soldecb";

    // XML node keys CLIENT
    static final String KEY_CLIENTS = "clients"; // parent node
    static final String KEY_CLIENT = "client";
    static final String KEY_ACTIF = "actif";
    static final String KEY_COMPTEDEFAULTNFC = "comptedefaultnfc";
    static final String KEY_DATEDENAISSANCE = "datedenaissance";
    static final String KEY_EMAIL = "email";
    static final String KEY_NUMEROT = "telephone";
    static final String KEY_IDCLIENT = "idclient";
    static final String KEY_IDENTIFIANT = "identifiant";
    static final String KEY_MOTDEPASSE = "motdepasse";
    static final String KEY_NOM = "nom";
    static final String KEY_PRENOM = "prenom";
    static final String KEY_NUMERORUE = "numerorue";
    static final String KEY_PAYS = "pays";
    static final String KEY_CODEPOSTAL = "codepostal";
    static final String KEY_RUE = "rue";
    static final String KEY_VILLE = "ville";
}
