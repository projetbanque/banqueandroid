package com.example.lmarchand.mabanquembds.service;

import com.example.lmarchand.mabanquembds.models.Client;
import com.example.lmarchand.mabanquembds.models.CompteBancaire;
import com.example.lmarchand.mabanquembds.models.CompteBeneficiaire;
import com.example.lmarchand.mabanquembds.models.Transaction;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface BanqueService {
    @Headers("Accept: application/json")
    @POST("transactions")
    Call<Void> createTransaction(@Body Transaction transaction);

    @GET("ws_entity.comptebancaire/compteclient/{id}/{mdp}")
    @Headers("Accept: application/json")
    Call<List<CompteBancaire>> getComptes(@Path("id") String identifiant, @Path("mdp") String mdp);

    @GET("ws_entity.comptebancaire/comptebeneficiaireclient/{id}/{mdp}")
    @Headers("Accept: application/json")
    Call<List<CompteBeneficiaire>> getBeneficiaires(@Path("id") String identifiant, @Path("mdp") String mdp);

    @PUT("ws_entity.client/editclient/{id}/{mdp}")
    @Headers("Accept: application/json")
    Call<Void> editClient(@Path("id") String identifiant,@Path("mdp") String mdp, @Body Client entity);

}
