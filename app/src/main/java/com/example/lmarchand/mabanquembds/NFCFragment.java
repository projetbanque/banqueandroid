package com.example.lmarchand.mabanquembds;


import android.app.FragmentManager;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lmarchand.mabanquembds.models.CompteBancaire;
import com.example.lmarchand.mabanquembds.models.Transaction;
import com.example.lmarchand.mabanquembds.service.RequestHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.AlarmClock.EXTRA_MESSAGE;
import static com.example.lmarchand.mabanquembds.paramsWebService.IDENTIFIANT_CLIENT;
import static com.example.lmarchand.mabanquembds.paramsWebService.MOTDEPASSE_CLIENT;


public class NFCFragment extends Fragment{
    View myView;
    Button buttonTransferer;
    EditText montant;
    EditText libelle;

    Spinner spinnerMesComptes;

    List<CompteBancaire> comptes;

    Transaction transaction = new Transaction();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_nfc, container,false);

        spinnerMesComptes = (Spinner)  myView.findViewById(R.id.spinnerCompteNFC);
        afficherForm();

        buttonTransferer = myView.findViewById(R.id.buttonTransferer);
        buttonTransferer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());

                if (!nfcAdapter.isNdefPushEnabled())
                {
                    //Demander à l’utilisateur d’activer l’option Beam
                    Toast.makeText(getContext(),"Activer le NFC et l'option Android Beam!",Toast.LENGTH_LONG).show();
                }else{
                    montant = myView.findViewById(R.id.editTextMontant);
                    if (montant.getText().toString().equals("")){
                        Toast.makeText(getContext(),"Champs manquant !",Toast.LENGTH_LONG).show();
                    }else{
                        Date current = new Date();
                        transaction.setDatetransfert(current.getTime());
                        libelle = myView.findViewById(R.id.editTextLibelle);
                        transaction.setLibelle(libelle.getText().toString());
                        transaction.setSommetransferee(Double.parseDouble(montant.getText().toString()));
                        //transaction.setStatus(1);

                        RequestHelper.provideService().createTransaction(transaction).enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {

                                Intent intent = new Intent( getContext(), NFCActivity.class );
                                intent.putExtra("montant", montant.getText().toString());
                                intent.putExtra("libelle", libelle.getText().toString());
                                intent.putExtra("IBAN", transaction.getIdComptedebit().getIbancb());

                                startActivityForResult(intent, 0);
                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {
                                Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        });


                    }

                }
            }
        });
        return myView;
    }


    private void afficherForm() {
        RequestHelper.provideService().getComptes(IDENTIFIANT_CLIENT, MOTDEPASSE_CLIENT).enqueue(new Callback<List<CompteBancaire>>() {
            @Override
            public void onResponse(Call<List<CompteBancaire>> call, Response<List<CompteBancaire>> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
                    return;
                }
                comptes = response.body();
                List data = new ArrayList();
                for(CompteBancaire compteBancaire: comptes) {
                    data.add(compteBancaire.toString());
                }
                ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, data);
                spinnerMesComptes.setAdapter(adapter);

                spinnerMesComptes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        transaction.setIdComptedebit(comptes.get(position));
                        //astuce pour le NFC : permet d'avoir l'autorisation de creer une transaction
                        transaction.setIdComptecredit(comptes.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<CompteBancaire>> call, Throwable t) {
                Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
            }
        });

    }


}
