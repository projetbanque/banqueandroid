package com.example.lmarchand.mabanquembds.models;

import com.google.gson.annotations.Expose;

import java.util.Date;

public class Transaction {
    @Expose(serialize = false)
    private int id;
    private long datetransfert;
    private String libelle;
    private double sommetransferee;
    private int status;
    private CompteBancaire idComptecredit;
    private CompteBancaire idComptedebit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getDatetransfert() {
        return datetransfert;
    }

    public void setDatetransfert(long datetransfert) {
        this.datetransfert = datetransfert;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getSommetransferee() {
        return sommetransferee;
    }

    public void setSommetransferee(double sommetransferee) {
        this.sommetransferee = sommetransferee;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public CompteBancaire getIdComptecredit() {
        return idComptecredit;
    }

    public void setIdComptecredit(CompteBancaire idComptecredit) {
        this.idComptecredit = idComptecredit;
    }

    public CompteBancaire getIdComptedebit() {
        return idComptedebit;
    }

    public void setIdComptedebit(CompteBancaire idComptedebit) {
        this.idComptedebit = idComptedebit;
    }
}
