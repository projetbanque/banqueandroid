package com.example.lmarchand.mabanquembds;

import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.lmarchand.mabanquembds.paramsWebService.NOM_CLIENT;
import static com.example.lmarchand.mabanquembds.paramsWebService.PRENOM_CLIENT;

public class NFCActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback, NfcAdapter.OnNdefPushCompleteCallback{
String montant;
String libelle;
NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);

        Intent intent = getIntent();
        montant = intent.getStringExtra("montant");
        libelle = intent.getStringExtra("libelle");

        nfcAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        //Abonnement pour le callback de création du message
        nfcAdapter.setNdefPushMessageCallback(this , this);
        //Abonnement pour le callback de fin d’émission du message
        nfcAdapter.setOnNdefPushCompleteCallback(this, this);


    }

    @Override
    //Cette méthode est appelée quand le périphérique NFC P2P est détecté
    public NdefMessage createNdefMessage(NfcEvent event)
    {
        if (montant.toString().equals("")) {
            return null;
        }
        //Utilisation de la méthode crée précédemment :
        NdefMessage msg = createMyNdefMessage(NOM_CLIENT+" "+PRENOM_CLIENT+" veut vous envoyer "+montant+"€."+"\n"+"Motif: "+libelle, "text/plain");
        return msg;
    }

    private NdefMessage createMyNdefMessage(String text, String mimeType) {
        //Message de type MIME
        NdefMessage msg = new NdefMessage(NdefRecord.createMime(mimeType, text.getBytes()));
        return msg;
    }

    @Override
    //Cette méthode est appelée lorsque le message a été réceptionné
    public void onNdefPushComplete(NfcEvent event)
    {
        nfcAdapter.setNdefPushMessage(null, this);
        nfcAdapter.setNdefPushMessageCallback(null, this);
        nfcAdapter.setOnNdefPushCompleteCallback(null, this);
        //Notifier l’utilisateur
        Toast.makeText(getApplicationContext(),"Demande transmise",Toast.LENGTH_LONG).show();
        setContentView(R.layout.activity_espace_client);
        startActivity(new Intent(this,EspaceClientActivity.class));
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
////        PendingIntent intent = PendingIntent.getActivity(this, 0, new Intent(this,
////                getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
////        nfcAdapter.enableForegroundDispatch(this,intent, null,
////                null);
//        //Abonnement pour le callback de création du message
//        nfcAdapter.setNdefPushMessageCallback(this , this);
//        //Abonnement pour le callback de fin d’émission du message
//        nfcAdapter.setOnNdefPushCompleteCallback(this, this);
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (NfcAdapter.getDefaultAdapter(this) != null)
//            NfcAdapter.getDefaultAdapter(this).disableForegroundDispatch(this);
//        nfcAdapter.setNdefPushMessage(null, this);
//        nfcAdapter.setNdefPushMessageCallback(null, this);
//        nfcAdapter.setOnNdefPushCompleteCallback(null, this);
//    }

}
