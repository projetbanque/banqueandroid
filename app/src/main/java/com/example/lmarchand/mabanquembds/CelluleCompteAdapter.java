package com.example.lmarchand.mabanquembds;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CelluleCompteAdapter extends ArrayAdapter<CelluleCompte> {

    public CelluleCompteAdapter(Context context, List<CelluleCompte> comptes) {
        super(context, 0, comptes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_compte,parent, false);
        }

        CelluleCompteAdapter.CompteViewHolder viewHolder = (CelluleCompteAdapter.CompteViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new CelluleCompteAdapter.CompteViewHolder();
            viewHolder.nom = (TextView) convertView.findViewById(R.id.textViewNom);
            viewHolder.iban = (TextView) convertView.findViewById(R.id.textViewIBAN);
            viewHolder.solde = (TextView) convertView.findViewById(R.id.textViewSolde);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        CelluleCompte compte = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.nom.setText(compte.getNom());
        viewHolder.iban.setText(compte.getIban());
        viewHolder.solde.setText(compte.getSolde());
        return convertView;
    }

    private class CompteViewHolder{
        public TextView nom;
        public TextView iban;
        public TextView solde;
    }
}
