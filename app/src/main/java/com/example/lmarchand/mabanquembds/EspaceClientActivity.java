package com.example.lmarchand.mabanquembds;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class EspaceClientActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_espace_client);
        this.onItemSelected("nav_acceuil");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, new NFCFragment()).commit();

                }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        nfcAdapter.setNdefPushMessage(null, this);
        nfcAdapter.setNdefPushMessageCallback(null, this);
        nfcAdapter.setOnNdefPushCompleteCallback(null, this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.espace_client, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("A propos");
            builder.setMessage("Cette application a été créée dans le cadre du master MBDS de l'université de Nice-Sophia Antipolis par Lu ZHAO et Lore MARCHAND");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();

        if (id == R.id.nav_mescomptes) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MesComptesFragment()).commit();

        } else if (id == R.id.nav_acceuil) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new acceuilFragment()).commit();

        } else if (id == R.id.nav_mestransactions) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MesTransactionsFragment()).commit();
        } else if (id == R.id.nav_effectuerVirement) {
             fragmentManager.beginTransaction().replace(R.id.content_frame, new VirementFragment()).commit();
        } else if (id == R.id.nav_monprofil) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MonProfilFragment()).commit();

        } else if (id == R.id.nav_effectuerContact) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new Appel()).commit();

        } else if (id == R.id.nav_mabanque) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new maBanqueFragment()).commit();

        }else if (id == R.id.nav_deconnexion) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Deconnexion");
            builder.setMessage("Voulez-vous vous déconnecter?");

            builder.setPositiveButton("OUI", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(EspaceClientActivity.this,MainActivity.class));

                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("NON", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        nfcAdapter.setNdefPushMessage(null, this);
        nfcAdapter.setNdefPushMessageCallback(null, this);
        nfcAdapter.setOnNdefPushCompleteCallback(null, this);
    }

    public void onItemSelected(String item) {
        FragmentManager fragmentManager = getFragmentManager();

        if (item == "nav_mescomptes") {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MesComptesFragment()).commit();

        } else if (item == "nav_acceuil") {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new acceuilFragment()).commit();

        } else if (item == "nav_mestransactions") {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MesTransactionsFragment()).commit();
        } else if (item == "nav_effectuerVirement") {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new VirementFragment()).commit();
        } else if (item == "nav_monprofil") {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MonProfilFragment()).commit();

        } else if (item == "nav_effectuerContact") {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new Appel()).commit();

        } else if (item == "nav_mabanque") {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new maBanqueFragment()).commit();

        }
    }
}
