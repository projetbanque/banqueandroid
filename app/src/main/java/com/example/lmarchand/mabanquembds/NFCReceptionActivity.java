package com.example.lmarchand.mabanquembds;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;

import java.util.ArrayList;

import static com.example.lmarchand.mabanquembds.MainActivity.MY_APP_KEY_ID;
import static com.example.lmarchand.mabanquembds.MainActivity.MY_LOGIN_KEY;
import static com.example.lmarchand.mabanquembds.MainActivity.MY_PASSWORD_KEY;

import static com.example.lmarchand.mabanquembds.paramsWebService.IDENTIFIANT_CLIENT;
import static com.example.lmarchand.mabanquembds.paramsWebService.MOTDEPASSE_CLIENT;

public class NFCReceptionActivity extends AppCompatActivity {

    Button buttonAccepter;
    Button buttonRefuser;
    NfcAdapter mNfcAdapter;

    SharedPreferences settings;
    String text_ID;
    String text_password;

    //The array lists to hold our messages
    private ArrayList<String> messagesReceivedArray = new ArrayList<>();
    //Text boxes to add and display our messages
    private TextView txtReceivedMessages;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfcreception);

        settings = getSharedPreferences(MY_APP_KEY_ID,0);
        text_ID= settings.getString(MY_LOGIN_KEY,"");
        text_password= settings.getString(MY_PASSWORD_KEY,"");
        IDENTIFIANT_CLIENT=text_ID;
        MOTDEPASSE_CLIENT=text_password;



        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        txtReceivedMessages = (TextView) findViewById(R.id.textViewMessage);

        updateTextViews();

        if (getIntent().getAction().equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {
            handleNfcIntent(getIntent());
        }

        buttonAccepter = findViewById(R.id.buttonAccepter);
        buttonAccepter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //post BBD

                startActivity(new Intent(NFCReceptionActivity.this,EspaceClientActivity.class));
            }
        });

        buttonRefuser = findViewById(R.id.buttonRefuser);
        buttonRefuser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(NFCReceptionActivity.this,EspaceClientActivity.class));
            }
        });
    }

    private void handleNfcIntent(Intent NfcIntent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(NfcIntent.getAction())) {
            Parcelable[] receivedArray =
                    NfcIntent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            if (receivedArray != null) {
                messagesReceivedArray.clear();
                NdefMessage receivedMessage = (NdefMessage) receivedArray[0];
                NdefRecord[] attachedRecords = receivedMessage.getRecords();

                for (NdefRecord record : attachedRecords) {
                    String string = new String(record.getPayload());
                    //Make sure we don't pass along our AAR (Android Applicatoin Record)
                    if (string.equals(getPackageName())) {
                        continue;
                    }
                    messagesReceivedArray.add(string);
                }
                Toast.makeText(this, "Received " + messagesReceivedArray.size() +
                        " Messages", Toast.LENGTH_LONG).show();
                updateTextViews();
            } else {
                Toast.makeText(this, "Received Blank Parcel", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        handleNfcIntent(intent);
    }


    @Override
    public void onResume() {
        super.onResume();
        updateTextViews();
        handleNfcIntent(getIntent());
    }


    private void updateTextViews() {

        //Populate our list of messages we have received
        if (messagesReceivedArray.size() > 0) {
            for (int i = 0; i < messagesReceivedArray.size(); i++) {
                txtReceivedMessages.append(messagesReceivedArray.get(i));
                txtReceivedMessages.append("\n");
            }
        }
    }


}
