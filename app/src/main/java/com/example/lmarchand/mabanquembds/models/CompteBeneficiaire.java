package com.example.lmarchand.mabanquembds.models;

public class CompteBeneficiaire {
    private int id;
    private String nomscb;
    private CompteBancaire idComptebeneficiare;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomscb() {
        return nomscb;
    }

    public void setNomscb(String nomscb) {
        this.nomscb = nomscb;
    }

    public CompteBancaire getIdComptebeneficiare() {
        return idComptebeneficiare;
    }

    public void setIdComptebeneficiare(CompteBancaire idComptebeneficiare) {
        this.idComptebeneficiare = idComptebeneficiare;
    }

    @Override
    public String toString() {
        return idComptebeneficiare.toString();
    }
}
