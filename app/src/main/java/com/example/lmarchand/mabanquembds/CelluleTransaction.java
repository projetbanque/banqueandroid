package com.example.lmarchand.mabanquembds;

import android.widget.ImageView;

public class CelluleTransaction {
    private String nom;
    private String compte;
    private String nomA;
    private String compteA;
    private String date;
    private String argent;
    private String libelle;

    public CelluleTransaction(String nom, String compte, String nomA, String compteA, String date, String argent, String libelle) {
        this.nom = nom;
        this.compte = compte;
        this.nomA = nomA;
        this.compteA = compteA;
        this.date = date;
        this.argent = argent;
        this.libelle = libelle;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCompte() {
        return compte;
    }

    public void setCompte(String compte) {
        this.compte = compte;
    }

    public String getNomA() {
        return nomA;
    }

    public void setNomA(String nomA) {
        this.nomA = nomA;
    }

    public String getCompteA() {
        return compteA;
    }

    public void setCompteA(String compteA) {
        this.compteA = compteA;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getArgent() {
        return argent;
    }

    public void setArgent(String argent) {
        this.argent = argent;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}