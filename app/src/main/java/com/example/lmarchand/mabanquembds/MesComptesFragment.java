package com.example.lmarchand.mabanquembds;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.lmarchand.mabanquembds.paramsWebService.*;


public class MesComptesFragment extends Fragment {

    ListView listCompte;

    View myView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_mes_comptes, container,false);

        listCompte = (ListView)  myView.findViewById(R.id.listViewCompte);

        afficherListeCompte();

        return myView;
    }

    private List<CelluleCompte> genererCompte(){
        List<CelluleCompte> comptes = new ArrayList<CelluleCompte>();
        comptes.add(new CelluleCompte( "FAKE Compte Courant", "FAKE FRE75fREZ75fd", "FAKE 500"));
        comptes.add(new CelluleCompte( "FAKE Livret A", "FAKE FRE75fRGRSG", "FAKE 2000"));
        comptes.add(new CelluleCompte( "FAKE PEL", "FAKE FRE75fREGRSGH", "FAKE 10000"));

        return comptes;
    }



    private void afficherListeCompte(){
        try {
            //requete webservice
            XMLParser parser = new XMLParser();
            String xml = parser.getXml(URL_START + "ws_entity.comptebancaire/compteclient/" + IDENTIFIANT_CLIENT + "/" + MOTDEPASSE_CLIENT); // getting XML
            Document doc = parser.getDomElement(xml); // getting DOM element

            NodeList nl = doc.getElementsByTagName(KEY_COMPTEBANCAIRE);
            List<CelluleCompte> comptes = new ArrayList<CelluleCompte>();
            // looping through all item nodes <item>
            for (int i = 0; i < nl.getLength(); i++) {
                Element e = (Element) nl.item(i);
                comptes.add(new CelluleCompte(parser.getValue(e, KEY_NOMCB), parser.getValue(e, KEY_IBANCB), parser.getValue(e, KEY_SOLDECB)));
            }

            CelluleCompteAdapter adapter = new CelluleCompteAdapter(getContext(), comptes);
            listCompte.setAdapter(adapter);
        }catch (Exception e){
            Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
            List<CelluleCompte> comptes = genererCompte();
            CelluleCompteAdapter adapter = new CelluleCompteAdapter(getContext(), comptes);
            listCompte.setAdapter(adapter);
        }
    }



}
