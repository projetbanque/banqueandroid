package com.example.lmarchand.mabanquembds;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

public class Appel extends Fragment {

    Button appel;
    Button mail;



    private View parentView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.appel, container, false);
        setUpViews();



////fonction mail-2(prêt)
        mail=(Button) parentView.findViewById(R.id.mail);
        mail.setOnClickListener(new View.OnClickListener() {

            public void onClick(View parentView) {
                Intent i = new Intent(getActivity(), mail.class);
                //getActivity().startActivity(i);
                startActivityForResult(i, 0);
            }
        });





        return parentView;
    }
//fonction appel
    private void setUpViews() {
        parentView.findViewById(R.id.appel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alerDialog = new AlertDialog.Builder(getActivity());

                alerDialog.setTitle("Voullez-vous appeler à votre agence?");
                alerDialog.setMessage("+33 6 10 83 93 87");

                alerDialog.setNegativeButton("Non",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                dialogInterface.cancel();

                            }
                        });
                alerDialog.setPositiveButton("Oui",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {

                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:0610839387"));
                                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(callIntent);

                            }
                        });
                alerDialog.show();
            }
        });
    }}