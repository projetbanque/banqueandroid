package com.example.lmarchand.mabanquembds;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import static com.example.lmarchand.mabanquembds.paramsWebService.*;


public class MonProfilFragment extends Fragment {

    private View myView;
    Button buttonChange;

    Scroller scrollView2;
    TextView textViewPrenom1;
    EditText editTextPrenom;
    TextView textViewNom;
    EditText editTextNom;
    TextView textViewVille;
    EditText editTextVille;
    TextView textViewCodePostal;
    EditText editTextCodePostal;
    TextView textViewRue;
    EditText editTextRue;
    TextView textViewNoRue;
    EditText editTextNoRue;
    TextView textViewPays;
    EditText editTextPays;
    TextView textViewMail;
    EditText editTextMail;
    TextView textViewNumeroT;
    EditText editTextNumeroT;
    TextView textViewMDP;
    EditText editTextMDP;
    TextView textViewNais;
    EditText editTextNais;
    TextView textViewIden;
    EditText editTextIden;
    TextView textViewAddres;
    EditText editTextAddres;


    String resultat;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_mon_profil, container,false);

        buttonChange=(Button) myView.findViewById(R.id.buttonChange);
        buttonChange.setOnClickListener(new View.OnClickListener() {

                public void onClick(View parentView) {
                    Intent i = new Intent(getActivity(), MonProfilModifActivity.class);
                    //getActivity().startActivity(i);
                    startActivityForResult(i, 0);
                }
            });

        try {
            //Appel web service
            XMLParser parser = new XMLParser();
            String xml = parser.getXml(URL_LOGIN + IDENTIFIANT_CLIENT + "/" + MOTDEPASSE_CLIENT); // getting XML
            Document doc = parser.getDomElement(xml); // getting DOM element

            NodeList nl = doc.getElementsByTagName(KEY_CLIENT);
            Element e = (Element) nl.item(0);

            textViewNom = (TextView) myView.findViewById(R.id.textViewNom);
            textViewNom.setText(parser.getValue(e, KEY_NOM) + " " + parser.getValue(e, KEY_PRENOM));


            textViewMail = (TextView) myView.findViewById(R.id.textViewMail);
            textViewMail.setText(parser.getValue(e, KEY_EMAIL));

            textViewIden = (TextView) myView.findViewById(R.id.textViewIden);
            textViewIden.setText(parser.getValue(e, KEY_IDENTIFIANT));

            textViewMDP = (TextView) myView.findViewById(R.id.textViewMDP);
            textViewMDP.setText("****");

            textViewNumeroT = (TextView) myView.findViewById(R.id.textViewNumeroT);
            textViewNumeroT.setText(parser.getValue(e, KEY_NUMEROT));

            textViewNais = (TextView) myView.findViewById(R.id.textViewNais);
            textViewNais.setText(parser.getValue(e, KEY_DATEDENAISSANCE).substring(0, 10));

            textViewAddres = (TextView) myView.findViewById(R.id.textViewAddres);
            textViewAddres.setText(parser.getValue(e, KEY_NUMERORUE) + " " + parser.getValue(e, KEY_RUE) + " " + parser.getValue(e, KEY_CODEPOSTAL) + " " + parser.getValue(e, KEY_VILLE));

            textViewPays = (TextView) myView.findViewById(R.id.textViewPays);
            textViewPays.setText(parser.getValue(e, KEY_PAYS));
        }catch (Exception e){
            Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
        }

        return myView;
    }



}
