package com.example.lmarchand.mabanquembds;

import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import android.app.ProgressDialog;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import static com.example.lmarchand.mabanquembds.paramsWebService.*;

public class MainActivity extends AppCompatActivity {
    Button buttonLogin;
    EditText password;
    EditText identifiant;
    CheckBox sauvegarderDonnees;

    // Progress Dialog Object
    ProgressDialog prgDialog;


    public  final static String MY_APP_KEY_ID ="MY_APP_KEY_ID";

    public  final static String MY_LOGIN_KEY ="MY_LOGIN_KEY";
    public final static String MY_PASSWORD_KEY="MY_PASSWORD_KEY";

    SharedPreferences settings;
    String text_ID;
    String text_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        nfcAdapter.setNdefPushMessage(null, this);
        nfcAdapter.setNdefPushMessageCallback(null, this);
        nfcAdapter.setOnNdefPushCompleteCallback(null, this);

        settings = getSharedPreferences(MY_APP_KEY_ID,0);

        text_ID= settings.getString(MY_LOGIN_KEY,"");

        identifiant = findViewById(R.id.editTextId);
        identifiant.setText(text_ID);

        text_password= settings.getString(MY_PASSWORD_KEY,"");

        password = findViewById(R.id.editTextPassword);
        password.setText(text_password);

        buttonLogin = findViewById(R.id.buttonLogin);
        /**
         * Method gets triggered when Login button is clicked
         */
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                password = findViewById(R.id.editTextPassword);
                identifiant = findViewById(R.id.editTextId);


                XMLParser parser = new XMLParser();
                try{
                    String xml = parser.getXml(URL_LOGIN+identifiant.getText()+"/"+password.getText()); // getting XML

                    if (xml==null){
                        Toast.makeText(getApplicationContext(),"Mauvais identifiant ET/OU Mot de Passe // OU PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
                        if (password.getText().toString().equals("fake")&&identifiant.getText().toString().equals("fake")){
                            startActivity(new Intent(MainActivity.this,EspaceClientActivity.class));
                        }
                    }else{
                        IDENTIFIANT_CLIENT=identifiant.getText().toString();
                        MOTDEPASSE_CLIENT=password.getText().toString();

                        Document doc = parser.getDomElement(xml); // getting DOM element
                        NodeList nl = doc.getElementsByTagName(KEY_CLIENT);
                        Element e = (Element) nl.item(0);
                        NOM_CLIENT=parser.getValue(e, KEY_NOM);
                        PRENOM_CLIENT=parser.getValue(e, KEY_PRENOM);

                        sauvegarderDonnees = findViewById(R.id.checkBoxSauvegarde);
                        if (sauvegarderDonnees.isChecked()){
                            text_ID=identifiant.getText().toString();
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString(MY_LOGIN_KEY, text_ID);
                            editor.commit();

                            text_password=password.getText().toString();
                            editor = settings.edit();
                            editor.putString(MY_PASSWORD_KEY, text_password);
                            editor.commit();
                        }
                        else{
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString(MY_LOGIN_KEY, "");
                            editor.commit();

                            editor = settings.edit();
                            editor.putString(MY_PASSWORD_KEY, "");
                            editor.commit();
                        }

                        startActivity(new Intent(MainActivity.this,EspaceClientActivity.class));
                    }


                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Mauvais identifiant ET/OU Mot de Passe // OU PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
                    if (password.getText().toString().equals("fake")&&identifiant.getText().toString().equals("fake")){
                        startActivity(new Intent(MainActivity.this,EspaceClientActivity.class));
                    }
                }

            }
        });

        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        nfcAdapter.setNdefPushMessage(null, this);
        nfcAdapter.setNdefPushMessageCallback(null, this);
        nfcAdapter.setOnNdefPushCompleteCallback(null, this);
    }

//    /**
//     * Method that performs RESTful webservice invocations
//     *
//     * @param params
//     */
//
//    public void invokeWS(RequestParams params){
//        // Show Progress Dialog
//        prgDialog.show();
//        // Make RESTful webservice call using AsyncHttpClient object
//        AsyncHttpClient client = new AsyncHttpClient();
//        //client.get("http://192.168.1.32:8080/WebServiceBanqueMarchandZhao/webresources/entity.client/login",params ,new AsyncHttpResponseHandler()
//        client.get("http://192.168.1.32:8080/WebServiceBanqueMarchandZhao/webresources/entity.client/login/lolo/lolo",new AsyncHttpResponseHandler(){
//            // When the response returned by REST has Http response code '200'
//            @Override
//            public void onSuccess(String response) {
//                // Hide Progress Dialog
//                prgDialog.hide();
//                try {
//                    // JSON Object
//                    JSONObject obj = new JSONObject(response);
//                    // When the JSON response has status boolean value assigned with true
//                    if(obj.getBoolean("status")){
//                        Toast.makeText(getApplicationContext(), "You are successfully logged in!", Toast.LENGTH_LONG).show();
//                        // Navigate to Home screen
//                        //startActivity(new Intent(MainActivity.this,MesTransactionsActivity.class));
//                        startActivity(new Intent(MainActivity.this,EspaceClientActivity.class));
//                    }
//                    // Else display error message
//                    else{
//                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
//                    }
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
//                    e.printStackTrace();
//
//                }
//            }
//            // When the response returned by REST has Http response code other than '200'
//            @Override
//            public void onFailure(int statusCode, Throwable error,
//                                  String content) {
//                // Hide Progress Dialog
//                prgDialog.hide();
//                // When Http response code is '404'
//                if(statusCode == 404){
//                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
//                }
//                // When Http response code is '500'
//                else if(statusCode == 500){
//                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
//                }
//                // When Http response code other than 404, 500
//                else{
//                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//    }
//


}
