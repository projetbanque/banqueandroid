package com.example.lmarchand.mabanquembds;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static com.example.lmarchand.mabanquembds.paramsWebService.NOM_CLIENT;

public class CelluleTransactionAdapter extends ArrayAdapter<CelluleTransaction>{
    //tweets est la liste des models à afficher
    public CelluleTransactionAdapter(Context context, List<CelluleTransaction> transactions) {
        super(context, 0, transactions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_transaction,parent, false);
        }

        TransactionViewHolder viewHolder = (TransactionViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new TransactionViewHolder();
            viewHolder.nom = (TextView) convertView.findViewById(R.id.textViewNom);
            viewHolder.compte = (TextView) convertView.findViewById(R.id.textViewCompte);
            viewHolder.nomA = (TextView) convertView.findViewById(R.id.textViewNomA);
            viewHolder.compteA = (TextView) convertView.findViewById(R.id.textViewCompteA);
            viewHolder.date = (TextView) convertView.findViewById(R.id.textViewDate);
            viewHolder.argent = (TextView) convertView.findViewById(R.id.textViewArgentTransfere);
            viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
            viewHolder.libelle = (TextView) convertView.findViewById(R.id.textViewLibelle);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        CelluleTransaction transaction = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.nom.setText(transaction.getNom());
        viewHolder.nomA.setText(transaction.getNomA());
        viewHolder.compte.setText(transaction.getCompte());
        viewHolder.compteA.setText(transaction.getCompteA());
        viewHolder.date.setText(transaction.getDate());
        viewHolder.argent.setText(transaction.getArgent());
        viewHolder.libelle.setText(transaction.getLibelle());

        if (!NOM_CLIENT.equals(viewHolder.nom.getText().toString())){
            viewHolder.avatar.setImageResource(R.drawable.recevoir);
        }else{
            viewHolder.avatar.setImageResource(R.drawable.envoyer);
        }


        return convertView;
    }

    private class TransactionViewHolder{
        public TextView nom;
        public TextView compte;
        public TextView nomA;
        public TextView compteA;
        public TextView date;
        public TextView argent;
        public ImageView avatar;
        public TextView libelle;

    }
}
