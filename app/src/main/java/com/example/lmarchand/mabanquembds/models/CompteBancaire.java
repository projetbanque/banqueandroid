package com.example.lmarchand.mabanquembds.models;

import java.sql.Date;

public class CompteBancaire {
    int id;
    private int actifcb;
    private String ibancb;
    private String nomcb;
    private double soldecb;
    private double transactionmaxcb;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActifcb() {
        return actifcb;
    }

    public void setActifcb(int actifcb) {
        this.actifcb = actifcb;
    }

    public String getIbancb() {
        return ibancb;
    }

    public void setIbancb(String ibancb) {
        this.ibancb = ibancb;
    }

    public String getNomcb() {
        return nomcb;
    }

    public void setNomcb(String nomcb) {
        this.nomcb = nomcb;
    }

    public double getSoldecb() {
        return soldecb;
    }

    public void setSoldecb(double soldecb) {
        this.soldecb = soldecb;
    }

    public double getTransactionmaxcb() {
        return transactionmaxcb;
    }

    public void setTransactionmaxcb(double transactionmaxcb) {
        this.transactionmaxcb = transactionmaxcb;
    }

    @Override
    public String toString() {
        return nomcb + " " + ibancb;
    }
}
