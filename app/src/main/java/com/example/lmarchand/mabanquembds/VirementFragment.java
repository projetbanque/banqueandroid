package com.example.lmarchand.mabanquembds;

import android.app.FragmentManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lmarchand.mabanquembds.models.CompteBancaire;
import com.example.lmarchand.mabanquembds.models.CompteBeneficiaire;
import com.example.lmarchand.mabanquembds.models.Transaction;
import com.example.lmarchand.mabanquembds.service.RequestHelper;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.lmarchand.mabanquembds.paramsWebService.IDENTIFIANT_CLIENT;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_COMPTEBANCAIRE;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_IBANCB;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_NOMCB;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_NOMSCB;
import static com.example.lmarchand.mabanquembds.paramsWebService.KEY_SAUVEGARDECOMPTEBENEFICIAIRE;
import static com.example.lmarchand.mabanquembds.paramsWebService.MOTDEPASSE_CLIENT;
import static com.example.lmarchand.mabanquembds.paramsWebService.URL_START;


public class VirementFragment extends android.app.Fragment {

    View myView;
    Spinner spinnerMesComptes;
    Spinner spinnerComptesBeneficiaire;
    Button buttonTransferer;

    List<CompteBancaire> comptes;
    List<CompteBeneficiaire> beneficiaires;

    Transaction transaction = new Transaction();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_virement, container,false);

        spinnerMesComptes = (Spinner)  myView.findViewById(R.id.spinnerCompte);
        spinnerComptesBeneficiaire = (Spinner)  myView.findViewById(R.id.spinnerBeneficiaire);
        afficherInformationVirement();

        buttonTransferer = (Button) myView.findViewById(R.id.buttonTransferer);
        buttonTransferer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText libelle = myView.findViewById(R.id.editTextLibelle);
                EditText montant = myView.findViewById(R.id.editTextMontant);
                if (montant.getText().toString().equals("")){
                    Toast.makeText(getContext(),"Champs manquant !",Toast.LENGTH_LONG).show();
                }else{
                    Date current = new Date();
                    transaction.setDatetransfert(current.getTime());
                    transaction.setLibelle(libelle.getText().toString());
                    transaction.setSommetransferee(Double.parseDouble(montant.getText().toString()));

                    RequestHelper.provideService().createTransaction(transaction).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Toast.makeText(getContext(),"Transaction effectuée",Toast.LENGTH_LONG).show();
                            FragmentManager fm = getActivity().getFragmentManager();
                            fm.beginTransaction().replace(R.id.content_frame, new acceuilFragment()).commit();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });

        return myView;
    }

    private void afficherInformationVirement(){

        RequestHelper.provideService().getComptes(IDENTIFIANT_CLIENT, MOTDEPASSE_CLIENT).enqueue(new Callback<List<CompteBancaire>>() {
            @Override
            public void onResponse(Call<List<CompteBancaire>> call, Response<List<CompteBancaire>> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
                    return;
                }
                comptes = response.body();
                List data = new ArrayList();
                for(CompteBancaire compteBancaire: comptes) {
                    data.add(compteBancaire.toString());
                }
                ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, data);
                spinnerMesComptes.setAdapter(adapter);

                spinnerMesComptes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        transaction.setIdComptedebit(comptes.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<CompteBancaire>> call, Throwable t) {
                Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
            }
        });

        RequestHelper.provideService().getBeneficiaires(IDENTIFIANT_CLIENT, MOTDEPASSE_CLIENT).enqueue(new Callback<List<CompteBeneficiaire>>() {
            @Override
            public void onResponse(Call<List<CompteBeneficiaire>> call, Response<List<CompteBeneficiaire>> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
                    return;
                }
                beneficiaires = response.body();
                List data = new ArrayList();
                if(beneficiaires == null) return;
                for(CompteBeneficiaire compteBancaire: beneficiaires) {
                    data.add(compteBancaire.toString());
                }
                ArrayAdapter adapterBeneficiaire = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, data);
                spinnerComptesBeneficiaire.setAdapter(adapterBeneficiaire);

                spinnerComptesBeneficiaire.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        transaction.setIdComptecredit(beneficiaires.get(position).getIdComptebeneficiare());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<CompteBeneficiaire>> call, Throwable t) {
                Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
            }
        });


        /*try {

            //requete webservice
            XMLParser parser = new XMLParser();
            String xml = parser.getXml(URL_START + "ws_entity.comptebancaire/compteclient/" + IDENTIFIANT_CLIENT + "/" + MOTDEPASSE_CLIENT); // getting XML
            Document doc = parser.getDomElement(xml); // getting DOM element

            NodeList nl = doc.getElementsByTagName(KEY_COMPTEBANCAIRE);
            List comptes = new ArrayList();
            // looping through all item nodes <item>
            for (int i = 0; i < nl.getLength(); i++) {
                Element e = (Element) nl.item(i);
                comptes.add(parser.getValue(e, KEY_NOMCB)+" "+parser.getValue(e, KEY_IBANCB));
            }
            ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, comptes);
            spinnerMesComptes.setAdapter(adapter);
            spinnerMesComptes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                }
            });


            //requete webservice
            xml = parser.getXml(URL_START+"beneficiaires/comptebeneficiaireclient/"+IDENTIFIANT_CLIENT+"/"+MOTDEPASSE_CLIENT); // getting XML
            doc = parser.getDomElement(xml); // getting DOM element

            nl = doc.getElementsByTagName(KEY_SAUVEGARDECOMPTEBENEFICIAIRE);
            List comptesBeneficiaires = new ArrayList();
            // looping through all item nodes <item>
            for (int i = 0; i < nl.getLength(); i++) {
                Element e = (Element) nl.item(i);
                comptesBeneficiaires.add(parser.getValue(e, KEY_NOMSCB)+" "+parser.getValue(e, KEY_IBANCB));
            }
            ArrayAdapter adapterBeneficiaire = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, comptesBeneficiaires);
            spinnerComptesBeneficiaire.setAdapter(adapterBeneficiaire);


        }catch (Exception e){
            Toast.makeText(getContext(),"PAS DE CONNECTION INTERNET / WEBSERVICE NON ACTIVE",Toast.LENGTH_LONG).show();
        }*/
    }


}