package com.example.lmarchand.mabanquembds;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;


public class acceuilFragment extends android.app.Fragment {

    View myView;
    ImageButton compte;
    ImageButton transaction;
    ImageButton virement;
    ImageButton profil;
    ImageButton contact;
    ImageButton maBanque;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_acceuil, container,false);

//        Button mabanque1=(Button)myView.findViewById(R.id.maBanque);
//
//        maBanque = myView.findViewById(R.id.maBanque);
//        maBanque.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                  //méthod 1
//                FragmentTransaction fr=getFragmentManager().beginTransaction();
//                fr.replace(R.id.fragment_container,new maBanqueFragment());
//                fr.commit();
//
//                  //méthod 2
//                FragmentManager fragmentManager = getChildFragmentManager();
//                fragmentManager.beginTransaction().replace(R.id.content_frame, new maBanqueFragment()).commit();
//                getChildFragmentManager().beginTransaction().replace(R.id.content_frame, new maBanqueFragment()).commit();
//
//            }
//        }

//        );


          // //méthod 3
//        public void onItemSelected(String item) {
//            FragmentManager fragmentManager = getFragmentManager();
//
//            if (item == "nav_mescomptes") {
//                fragmentManager.beginTransaction().replace(R.id.content_frame, new MesComptesFragment()).commit();
//
//            } else if (item == "nav_acceuil") {
//                fragmentManager.beginTransaction().replace(R.id.content_frame, new acceuilFragment()).commit();
//
//            } else if (item == "nav_mestransactions") {
//                fragmentManager.beginTransaction().replace(R.id.content_frame, new MesTransactionsFragment()).commit();
//            } else if (item == "nav_effectuerVirement") {
//            } else if (item == "nav_monprofil") {
//                fragmentManager.beginTransaction().replace(R.id.content_frame, new MonProfilFragment()).commit();
//
//            } else if (item == "nav_effectuerContact") {
//                fragmentManager.beginTransaction().replace(R.id.content_frame, new Appel()).commit();
//
//            } else if (item == "nav_mabanque") {
//                fragmentManager.beginTransaction().replace(R.id.content_frame, new maBanqueFragment()).commit();
//
//            }
//        }
//    }

       // //méthod 4--réussir transferer fragment1 à fragment2 pour ImageButton

        maBanque = (ImageButton)myView.findViewById(R.id.maBanque);
        maBanque.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new maBanqueFragment()).commit();
            }
        });



        contact = (ImageButton)myView.findViewById(R.id.contact);
        contact.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new Appel()).commit();
            }
        });

        profil = (ImageButton)myView.findViewById(R.id.profil);
        profil.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new MonProfilFragment()).commit();
            }
        });

        transaction = (ImageButton)myView.findViewById(R.id.transaction);
        transaction.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new MesTransactionsFragment()).commit();
            }
        });

        compte = (ImageButton)myView.findViewById(R.id.compte);
        compte.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new MesComptesFragment()).commit();
            }
        });

        virement = (ImageButton)myView.findViewById(R.id.virement);
        virement.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                fm.beginTransaction().replace(R.id.content_frame, new VirementFragment()).commit();
            }
        });





        return myView;
    }


}
